#include <iostream>
#include <omp.h>
#include <complex>
#define COMPLEX std::complex<double>

#ifdef USE_AOCL
#include "lapacke.h"
#define INT int
#else /* MKL */
#include <mkl.h>
#define INT MKL_INT
#endif

int main(int argc, char *argv[]) {
  double t0, t1, t2, t3;

  if (argc != 2) 
    {
        printf("Usage: %s <matrix_size>\n", argv[0]);
        return 1;
    }

    // Define matrix size
    INT N = atol(argv[1]);  // Matrix size is very large
    INT lda = N;

    printf("Matrix size: %lld\n", N);
    printf("sizeof(COMPLEX) = %lu\n", sizeof(COMPLEX));
    printf("sizeof(INT) = %lu\n", sizeof(INT));
    
    // Allocate memory for the matrix (Example initialization)
    COMPLEX *A = (COMPLEX *)malloc(N * N * sizeof(COMPLEX));
    double *lambda = (double *)malloc(N * sizeof(double));
    COMPLEX *X = (COMPLEX *)malloc(N * N * sizeof(COMPLEX));
    
    t0 = omp_get_wtime();

    // Initialize the matrix
    INT j;
#pragma omp parallel for
    for (INT i = 0; i < N-1; ++i) {
      j=i+1;
      A[i + j*N] = std::complex<double>(1.0,  1.0);
      A[j + i*N] = std::complex<double>(1.0, -1.0);
    }

    t1 = omp_get_wtime();

    int info;
    char jobz = 'V';  // V - Compute eigenvalues and eigenvectors; N - Compute eigenvalues only
    char uplo = 'U';  // Upper triangle of A is stored

    // Perform eigenvalue decomposition
#ifdef USE_AOCL
    info = LAPACKE_zheev(LAPACK_COL_MAJOR, jobz, uplo, N, (__complex__ double*) A, lda, lambda);
#else /* MKL */
    info = LAPACKE_zheev(LAPACK_COL_MAJOR, jobz, uplo, N, (lapack_complex_double*) A, lda, lambda);
    // the functions below have also been tested with the similar results
    //info = LAPACKE_zheevd(LAPACK_COL_MAJOR, jobz, uplo, N, A, lda, lambda);
    //info = LAPACKE_zheevd_64(LAPACK_COL_MAJOR, jobz, uplo, N, A, lda, lambda);
    //info = LAPACKE_zheevd_2stage (LAPACK_COL_MAJOR, jobz, uplo, N, A, lda, lambda);
#endif

    t2 = omp_get_wtime();

    if (info > 0) {
        std::cout << "The algorithm failed to compute eigenvalues." << std::endl;
    } else {
        std::cout << "Computation successful. First 10 Eigenvalues: " << std::endl;
        for (int i = 0; i < 10; i++) {
            std::cout << lambda[i] << std::endl;
        }
    }

    // Free resources
    free(A);
    free(lambda);
    free(X);

    t3 = omp_get_wtime();
    printf("\nTotal time: %f sec\n", t3-t0);
    printf("\nDiagonalization time: %f sec\n", t2-t1);

    return 0;
}
