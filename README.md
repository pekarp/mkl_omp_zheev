# MKL OpenMP zheev reprocuder

The reproducer is for Raven

## Slower reproducer with SLRUM

```
git clone https://gitlab.mpcdf.mpg.de/pekarp/mkl_omp_zheev.git
cd mkl_omp_zheev
bash compile.sh
sbatch job_raven.sh
```

Run times for different matrix sizes (N*N) are:

| N | runtime (hours) |
|----------|----------|
| 40k   |  0.575   |
| 90k   |  6.49    |
| 100k  |  9.15    |
| 110k  |  timeout in 24 h (expected ~12.2 h)   |


## Quicker interactive reproducer

### Installing htop

Unfortunately, `htop` is not installed on the Raven compute nodes (I think, more or less any machine can be used to see the problem). You can install it with the following command:
```
git clone https://github.com/htop-dev/htop.git
cd htop
./autogen.sh
mkdir build
cd build
../configure --prefix=$HOME/soft/htop/install
make -j 6
make install
```

### Submitting interactive job

#### 1. N=100,000 on 72 cores

For matrix size 100,000 and 72 cores everything works fine.
```
salloc --time=00:05:00 --nodes=1 --tasks-per-node=72 --cpus-per-task=1 --mail-type=all --mem=500000 # queue time 3-7 min upon testing
source compile.sh
export PATH=$HOME/soft/htop/install/bin:${PATH}
./main 100000 &
htop
```

![Alt text](screenshots/Screenshot_20240729_111934_size=100000,72_cores.png)


#### 2. N=100,001 on 72 cores


For matrix size 100,001 and 72 cores, the program is executed only on several cores with occasional spikes on other cores. The shell from the previous job can be reused.

```
killall -u $USER
./main 100001 &
htop
```

![Alt text](screenshots/Screenshot_20240729_112221,size=100001,72_cores.png)

#### 3. N=100,001 on 64 cores

There is even a cleaner reproducer is with 64 cores.
```
salloc --time=00:05:00 --nodes=1 --tasks-per-node=64 --cpus-per-task=1 --mail-type=all --mem=500000 # queue time 3-7 min upon testing
source compile.sh
export PATH=$HOME/soft/htop/install/bin:${PATH}
./main 100001 &
htop
```

![Alt text](screenshots/Screenshot_20240729_112743,size=100000,64_cores.png)