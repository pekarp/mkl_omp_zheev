
export OMP_NUM_THREADS=64
export OMP_PLACES=cores
export MKL_DYNAMIC=TRUE

export LD_LIBRARY_PATH=$MKLROOT/lib/intel64:$LD_LIBRARY_PATH # needed for libmkl_intel_ilp64.so.2

./main 10000
