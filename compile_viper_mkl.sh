#!/usr/bin/bash

#set -v

module purge
module load intel/2024.0
module load mkl/2024.0


FileName=main
icpx -O3 -qopenmp -DMKL_ILP64  -qmkl-ilp64=parallel -o $FileName $FileName.cpp
