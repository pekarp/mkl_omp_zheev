#!/bin/bash -l
# Standard output and error:
##SBATCH -o ./job.out.%j
##SBATCH -e ./job.err.%j
# Initial working directory:
#SBATCH -D ./
# Job name
#SBATCH -J mkl_omp_100k

#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=72

#SBATCH --mem=500000

#SBATCH --mail-type=ALL
#SBATCH --time=24:00:00

source compile.sh

export LD_LIBRARY_PATH=$MKLROOT/lib/intel64:$LD_LIBRARY_PATH # needed for libmkl_intel_ilp64.so.2

set -v

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PLACES=cores

#export MKL_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_DYNAMIC=TRUE

srun $FileName 100000
