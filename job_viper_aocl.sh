#!/bin/bash -l
# Standard output and error:
##SBATCH -o ./job.out.%j
##SBATCH -e ./job.err.%j
# Initial working directory:
#SBATCH -D ./
# Job name
#SBATCH -J aocl_omp_10k

#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=128

#SBATCH --mem=500000

#SBATCH --mail-type=ALL
#SBATCH --time=01:00:00

source compile_viper_aocl.sh

#export LD_LIBRARY_PATH=$MKLROOT/lib/intel64:$LD_LIBRARY_PATH # needed for libmkl_intel_ilp64.so.2

set -v

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PLACES=cores

#export MKL_NUM_THREADS=$SLURM_CPUS_PER_TASK
#export MKL_DYNAMIC=TRUE

srun ./main_aocl 10000
