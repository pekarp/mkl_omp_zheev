#!/usr/bin/bash

#set -v

module purge
module load gcc/13
module load aocl/gcc/4.2

LIBS="-lflame -lblis -L${AOCL_HOME}/lib -Wl,-rpath,${AOCL_HOME}/lib"

FileName=main
g++ -O3 -DUSE_AOCL -I${AOCL_HOME}/include -fopenmp -o ${FileName}_aocl $FileName.cpp ${LIBS}
